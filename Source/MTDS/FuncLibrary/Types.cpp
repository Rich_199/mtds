// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "MTDS/MTDS.h"
#include "MTDS/Interface/MTDS_IGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UMTDS_StateEffect> AddEffectClass,
	EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UMTDS_StateEffect* myEffect = Cast<UMTDS_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStakable)
					{
						int8 j = 0;
						TArray<UMTDS_StateEffect*> CurrentEffects;
						IMTDS_IGameActor* myInterface = Cast<IMTDS_IGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[i]->GetClass() !=AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}
					}
					else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{
						UMTDS_StateEffect* NewEffect = NewObject<UMTDS_StateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor);
						}
					}
				}
				i++;
			}
		}
	}
}
