// Fill out your copyright notice in the Description page of Project Settings.


#include "MTDS_StateEffect.h"
#include "MTDS/Character/MTDSHealthComponent.h"
#include "MTDS/Interface/MTDS_IGameActor.h"
#include "Kismet/GameplayStatics.h"

bool UMTDS_StateEffect::InitObject(AActor* Actor)
{

	myActor = Actor;

	IMTDS_IGameActor* myInterface = Cast<IMTDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UMTDS_StateEffect::DestroyObject()
{
	IMTDS_IGameActor* myInterface = Cast<IMTDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffects(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UMTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UMTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UMTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UMTDSHealthComponent* myHealthComp = Cast<UMTDSHealthComponent>(myActor->GetComponentByClass(UMTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
			if (AbilityEffect)
			{
				FName NameBoneToAttached;
				FVector Loc = FVector(0);

				AbilityEmitter = UGameplayStatics::SpawnEmitterAttached(AbilityEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator,EAttachLocation::SnapToTarget,false);
			}
		}
	}

	DestroyObject();
}

bool UMTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UMTDS_StateEffect_ExecuteTimer::DestroyObject, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UMTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator,EAttachLocation::SnapToTarget,false);
	}

	return true;
}

void UMTDS_StateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}


void UMTDS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		//UGameplayStatics::ApplyDamage(myActor, Power, nullptr, nullptr, nullptr);
		UMTDSHealthComponent* myHealthComp = Cast<UMTDSHealthComponent>(myActor->GetComponentByClass(UMTDSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}
