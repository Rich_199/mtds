// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MTDS/StateEffects/MTDS_StateEffect.h"
#include "MTDS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MTDS_API IMTDS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	// UFUNCTION(BlueprintCallable,BlueprintImplementableEvent,Category = "Event")
	// 	void AviableForEffectsBP();
	// UFUNCTION(BlueprintCallable, BlueprintImplementableEvent,Category = "Event")
	// 	bool AviableForEffects();

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UMTDS_StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffects (UMTDS_StateEffect* RemoveEffect);
	virtual void AddEffect(UMTDS_StateEffect* newEffect);
};
