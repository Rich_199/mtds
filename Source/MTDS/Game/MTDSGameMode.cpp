// Copyright Epic Games, Inc. All Rights Reserved.

#include "MTDSGameMode.h"
#include "MTDSPlayerController.h"
#include "MTDS/Character/MTDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMTDSGameMode::AMTDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMTDSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AMTDSGameMode::PlayerCharacterDead()
{
	
}
