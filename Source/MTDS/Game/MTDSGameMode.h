// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MTDSGameMode.generated.h"

UCLASS(minimalapi)
class AMTDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMTDSGameMode();

	void PlayerCharacterDead();
};



