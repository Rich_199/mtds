// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MTDS/Interface/MTDS_IGameActor.h"
#include "MTDS/StateEffects/MTDS_StateEffect.h"
#include "MTDS_EnviromentStructure.generated.h"

/**
 * 
 */
UCLASS()
class MTDS_API AMTDS_EnviromentStructure : public AActor, public IMTDS_IGameActor
{
	GENERATED_BODY()
	
public:
	//Sets defaults values for this actor`s properties
	AMTDS_EnviromentStructure();

protected:
	//Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	//Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	TArray<UMTDS_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffects(UMTDS_StateEffect* RemoveEffect) override;
	void AddEffect(UMTDS_StateEffect* newEffect) override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<UMTDS_StateEffect*> Effects;
};
