// Fill out your copyright notice in the Description page of Project Settings.


#include "MTDS_EnviromentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets defaults values


AMTDS_EnviromentStructure::AMTDS_EnviromentStructure()
{
	// Set this actor to call Tick() every frame. You can turn this off to improve performance if you don`t need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMTDS_EnviromentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMTDS_EnviromentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

EPhysicalSurface AMTDS_EnviromentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UMTDS_StateEffect*> AMTDS_EnviromentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void AMTDS_EnviromentStructure::RemoveEffects(UMTDS_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AMTDS_EnviromentStructure::AddEffect(UMTDS_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}
