// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldItemDefault.h"

AWorldItemDefault::AWorldItemDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AWorldItemDefault::BeginPlay()
{
	Super::BeginPlay();
}

void AWorldItemDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}
