// Copyright Epic Games, Inc. All Rights Reserved.

#include "MTDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MTDS, "MTDS" );

DEFINE_LOG_CATEGORY(LogMTDS)
 